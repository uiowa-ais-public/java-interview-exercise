Exercise 2
==========

For this exercise, I'm providing you an example set of data that is meant to
represent all applicants from the state of Iowa (it is test data).  The data
is provided in the test-data.csv file with the first row describing that particular
field.

We would like for you to answer the following questions from this data:

1. How many counties did not send students to the U of Iowa, and which counties were they?

2. Which counties send the most students to the U of Iowa per capita?

3. Identify something "interesting" in the data.  This is open ended and can be anything
   you want to draw attention to.  While the data is seeded in certain ways, we're not 
   looking for any particular answer (We're not hiding a single needle in the haystack that
   you are expected to find).


We are not providing all of the information you need to answer the questions, but you should
be able to research what you need to complete the exercise.

You can use any tools or approaches that you are comfortable with, including:

- writing an application to parse the file and answer the questions
- using shell tools to parse and analyze the file
- loading the data into a database and using SQL
- loading the data into Excel and using Excel tools
- or something we are not considering

Please describe the approach you used, and let me know if you have any questions.
