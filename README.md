Thanks for interviewing with us!

We think that seeing code you have written is an important part of the 
interview process, but we don't think that having you write code on the 
whiteboard or in a text editor while the interview team stares at you is
a realistic way to gauge your development and design skills.

Instead, please complete the following two exercises.  

In each directory is a README file which has the specs for the exercise.
We would hope that you shouldn't have to spend more than a couple of hours
on these exercises.  These are not meant to be "trick questions", please
feel free to make reasonable assumptions (and perhaps note those assumption
as comments) if the specs are unclear.

Once you have completed the exercises, zip up your version of the code and 
email it back to me (michael-alberhasky@uiowa.edu).

I'll share your code with the rest of the interview team once I receive it.

If you have any questions or problems, please let me know.


-Michael
